from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import sys
import os

version = __import__('consume').__version__

install_requires = [
	'flask',
	'python-dateutil',
]

dep_links = []

class Tox(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import tox
        errno = tox.cmdline(self.test_args)
        sys.exit(errno)

setup(
    name="consume",
    version=version,
    url='http://gitlab.com/secstate/consume',
    license='BSD',
    platforms=['OS Independent'],
    description="A homebrew API kit",
    author="Colin Powell",
    author_email='colin.powell@gmail.com',
    packages=find_packages(),
    install_requires=install_requires,
    dependency_links=dep_links,
    include_package_data=True,
    zip_safe=False,
    cmdclass={'test': Tox},
    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Flask',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    package_dir={
        'consume': 'consume',
        'consume/data': 'consume/data',
    },
    entry_points={
        'console_scripts': [
            'consume = consume:main',
        ],
    },
)
