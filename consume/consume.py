""""""
import os
import sqlite3
from datetime import datetime
from enum import Enum
from random import randint

from dateutil.parser import parse
from flask import Flask, g, jsonify

PATH = os.path.dirname(os.path.abspath(__file__))
DATABASE = os.path.join(PATH, "data/db.sqlite")


class COLUMNS(Enum):
    """Enum mapping db columns to list indicies"""

    Timestamp = 0
    eating = 1
    tasks = 2
    cleanliness = 3
    familytime = 4
    diary_and_exercise = 5


APP = Flask(__name__)


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


def query_db(query: str, args=(), one=False):
    "Given a db query, return rows found"
    cur = get_db().execute(query, args)
    values = cur.fetchall()
    cur.close()
    return (values[0] if values else None) if one else values


@APP.teardown_appcontext
def close_connection(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.close()
    if exception:
        print(exception)


def get_avg(column, rounding=2):
    "Given a column, return the average of all values"
    query = "SELECT avg({c}) FROM kpis".format(c=column)
    result = query_db(query, one=True)[0]
    if rounding:
        result = round(result, rounding)
    return result


def get_prev_avg(column, rounding=2):
    "Given a column, get average all values but the last 5 entries"
    query = """SELECT avg({c}) FROM kpis WHERE 'Timestamp' not in
               (select {c} from kpis order by 'Timestamp' desc limit 5)""".format(
        c=column
    )
    result = query_db(query, one=True)[0]
    if rounding:
        result = round(result, rounding)
    return result


def get_trend(earlier, latest):
    "Given two values, return whether we're trending up or down"
    if earlier == latest:
        return None
    return "up" if earlier < latest else "down"


@APP.route("/api/v1/kpis", methods=["GET"])
def get_data():
    data = {"food": [], "tasks": [], "house": [], "family_min": [], "diary_exer": []}
    all_rows = query_db("SELECT * FROM kpis")

    for row in all_rows:
        # convert datetime to ISO timestamp string
        timestamp = parse(row[0]).isoformat()

        data["food"].append({timestamp: row[COLUMNS.eating.value]})
        data["tasks"].append({timestamp: row[COLUMNS.tasks.value]})
        data["house"].append({timestamp: row[COLUMNS.cleanliness.value]})
        data["family_min"].append({timestamp: row[COLUMNS.familytime.value]})
        data["diary_exer"].append({timestamp: row[COLUMNS.diary_and_exercise.value]})

    data["food_avg"] = get_avg(COLUMNS.eating.name)
    data["tasks_avg"] = get_avg(COLUMNS.tasks.name)
    data["house_avg"] = get_avg(COLUMNS.cleanliness.name)
    data["family_hrs_avg"] = round(get_avg(COLUMNS.familytime.name) / 60, 1)
    data["diary_exer_percent"] = get_avg(COLUMNS.diary_and_exercise.name) * 100

    data["food_prev_avg"] = get_prev_avg(COLUMNS.eating.name)
    data["tasks_prev_avg"] = get_prev_avg(COLUMNS.tasks.name)
    data["house_prev_avg"] = get_prev_avg(COLUMNS.cleanliness.name)
    data["family_hrs_prev_avg"] = round(get_prev_avg(COLUMNS.familytime.name) / 60, 1)
    data["diary_exer_prev_percent"] = (
        get_prev_avg(COLUMNS.diary_and_exercise.name) * 100
    )

    data["food_trend"] = get_trend(data["food_prev_avg"], data["food_avg"])
    data["tasks_trend"] = get_trend(data["tasks_prev_avg"], data["tasks_avg"])
    data["house_trend"] = get_trend(data["house_prev_avg"], data["house_avg"])
    data["family_hrs_trend"] = get_trend(
        data["family_hrs_prev_avg"], data["family_hrs_avg"]
    )
    data["diary_exer_percent_trend"] = get_trend(
        data["diary_exer_prev_percent"], data["diary_exer_percent"]
    )
    return jsonify(data)


def quotes():
    quote_list = open(PATH + "/data/quotes.txt", "r").readlines()
    return [q.rstrip() for q in quote_list]


@APP.route("/api/v1/quotes/random", methods=["GET"])
def get_random_quote():
    quote_list = quotes()
    i = randint(0, len(quote_list) - 1)
    return jsonify([quote_list[i]])


@APP.route("/api/v1/quotes", methods=["GET"])
def get_quotes():
    return jsonify(quotes())


def meditations():
    meditations = {}
    mediation_list = open(PATH + "/data/meditations.txt", "r").readlines()
    for i in range(len(mediation_list)):
        meditations[i] = mediation_list[i].replace("\n", "").split("*")
    return meditations


@APP.route("/api/v1/meditations", methods=["GET"])
def get_meditations():
    return jsonify(meditations())


@APP.route("/api/v1/meditations/today", methods=["GET"])
def get_daily_meditation():
    today = int(datetime.now().strftime("%w"))
    todays = meditations()[today]
    i = randint(0, len(todays) - 1)
    return jsonify([todays[i]])


if __name__ == "__main__":
    APP.run(host="0.0.0.0")
