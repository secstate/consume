SHELL := /bin/bash

install:
	git clone https://github.com/ExpediaInceCommercePlatform/cyclotron.git
	virtualenv -p python3 venv
	venv/bin/pip install -r requirements.txt
	cp etc/cyclotron-config.js cyclotron/cyclotron-svc/config/config.js
	npm install

clean:
	rm -rf venv
	rm -rf cyclotron

run_api:
	python consume/app.py
	
run_cyclotron:
	node cyclotron/cyclotron-svc/app.js
